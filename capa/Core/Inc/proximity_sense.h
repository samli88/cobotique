/*
 * proximity_sense.h
 *
 *  Created on: 17 Jun 2020
 *      Author: Samira
 */

#ifndef INC_PROXIMITY_SENSE_H_
#define INC_PROXIMITY_SENSE_H_

float filter(uint16_t val, float prev, int n);
uint16_t L_Noise(uint8_t msb, uint8_t lsb);
void write_register(uint8_t, uint16_t);
uint32_t read_register(uint8_t msb_register, uint8_t lsb_register);
double standard_deviation(int* array, int length);

#endif /* INC_PROXIMITY_SENSE_H_ */
