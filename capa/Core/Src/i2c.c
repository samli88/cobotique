/**
  ******************************************************************************
  * File Name          : I2C.c
  * Description        : This file provides code for the configuration
  *                      of the I2C instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "i2c.h"

/* USER CODE BEGIN 0 */
HAL_StatusTypeDef ret;
uint8_t FDC = 0x2A<<1; // when addr pin tied to 0(GND) ; change to 0x2B<<1 when tied to 1(VDD)
uint8_t bufmsb[2], buflsb[2];

/* USER CODE END 0 */

I2C_HandleTypeDef hi2c1;

/* I2C1 init function */
void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_I2C_MspInit(I2C_HandleTypeDef* i2cHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(i2cHandle->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspInit 0 */

  /* USER CODE END I2C1_MspInit 0 */

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**I2C1 GPIO Configuration
    PB8     ------> I2C1_SCL
    PB9     ------> I2C1_SDA
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* I2C1 clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();
  /* USER CODE BEGIN I2C1_MspInit 1 */

  /* USER CODE END I2C1_MspInit 1 */
  }
}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* i2cHandle)
{

  if(i2cHandle->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspDeInit 0 */

  /* USER CODE END I2C1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_I2C1_CLK_DISABLE();

    /**I2C1 GPIO Configuration
    PB8     ------> I2C1_SCL
    PB9     ------> I2C1_SDA
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_9);

  /* USER CODE BEGIN I2C1_MspDeInit 1 */

  /* USER CODE END I2C1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
void write_register(uint8_t register_pointer, uint16_t register_value)
{
	uint8_t data[3];
	data[0] = register_pointer;     // address of the register
	data[1] = register_value>>8;    // MSB byte of 16bit data
	data[2] = register_value;       // LSB byte of 16bit data
	ret = HAL_I2C_Master_Transmit(&hi2c1, FDC, data, 3, 300);  // data is the start pointer of our array
}

uint32_t read_register(uint8_t msb_register, uint8_t lsb_register)
{
	uint16_t msb, lsb;
	uint32_t data;

	//Read MSB Data
	if(!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_6))
	{
		// first set the register pointer to the register wanted to be read
		ret = HAL_I2C_Master_Transmit(&hi2c1,FDC , &msb_register, 1, 100);  // note the & operator which gives us the address of the register_pointer variable
		// receive the 2 x 8bit data into the receive buffer
		HAL_I2C_Master_Receive(&hi2c1, FDC, bufmsb, 2, 100);
	}
	//Read LSB data
	if(!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_6))
	{
		ret = HAL_I2C_Master_Transmit(&hi2c1,FDC , &lsb_register, 1, 100);
		HAL_I2C_Master_Receive(&hi2c1, FDC, buflsb, 2, 100);
	}
	//Combine the data
	msb = (bufmsb[0]<<8)|bufmsb[1];
	lsb = (buflsb[0]<<8)|buflsb[1];
	data = (msb<<16)|(lsb);

	return data;
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
