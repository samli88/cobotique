/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "math.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DT 50
#define IT -1000
#define LEAKAGE 0.6
#define OBJECT -10000

#define DT2 400
#define IT2 -2000
#define LEAKAGE2 0.35
#define OBJECT2 -10000

#define CH0MSB 0x00
#define CH0LSB 0x01
#define CH1MSB 0x02
#define CH1LSB 0x03
#define CH2MSB 0x04
#define CH2LSB 0x05
#define CH3MSB 0x06
#define CH3LSB 0x07

#define RCOUNT_CH0 0x08
#define RCOUNT_CH1 0x09
#define RCOUNT_CH2 0x0A
#define RCOUNT_CH3 0x0B

#define SETTLECOUNT_CH0 0x10
#define SETTLECOUNT_CH1 0x11
#define SETTLECOUNT_CH2 0x12
#define SETTLECOUNT_CH3 0x13

#define CLK_DIVIDER_CH0 0x14
#define CLK_DIVIDER_CH1 0x15
#define CLK_DIVIDER_CH2 0x16
#define CLK_DIVIDER_CH3 0x17

#define DRIVE_CURRENT_CH0 0x1E
#define DRIVE_CURRENT_CH1 0x1F
#define DRIVE_CURRENT_CH2 0x20
#define DRIVE_CURRENT_CH3 0x21

#define STATUS 0x18
#define STATUS_CONFIG 0x19
#define MUX_CONFIG 0x1B
#define CONFIG 0x1A
#define RESET 0x1C

#define N 3
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

//GLOBAL VARIABLES
uint32_t sample=0, sample2=0;
int count=0, count2=0, init=0;
float avg=0, avg_prev=0, delta=0, avg2=0, avg2_prev=0, delta2=0;
float integral=0, integral_prev=0, integral2=0, integral2_prev=0;

//test
uint32_t samples[N]={0};
uint32_t samples2[N]={0};
float tavg=0, tavg_prev=0, tdelta=0;
float tavg2=0, tavg2_prev=0, tdelta2=0;

//REGISTER SETTINGS
uint16_t reset=0x0000;
uint16_t config=0x1e01;
uint16_t status= 0x404c; //0x804e
uint16_t errorConfig=0x0001;
uint16_t muxConfig=0xc20d;	//Read all channels - 0xa20d is for three channels [Ch0 to Ch3]

// CH0 Register Values
uint16_t clkDiv0=0x1002;
uint16_t rcount0=0xffff;
uint16_t settleCount0=0x0400;
uint16_t drvCurrent0=0xa380;// - 307uA

// CH1 Register Values
uint16_t clkDiv1=0x1001;
uint16_t rcount1=0xffff;
uint16_t settleCount1=0x0400;
uint16_t drvCurrent1= 0xfb80;//0x8800;

// CH2 Register Values
uint16_t clkDiv2=0x1001;
uint16_t rcount2=0xffff;
uint16_t settleCount2=0x0400;
uint16_t drvCurrent2=0xe820;//0x8a80;

// CH3 Register Values
uint16_t clkDiv3=0x1001;
uint16_t rcount3=0xffff;
uint16_t settleCount3=0x0400;
uint16_t drvCurrent3=0x8a80;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */
void write_register(uint8_t, uint16_t);
uint32_t read_register(uint8_t msb_register, uint8_t lsb_register);
int _write(int file, char *ptr, int len);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_I2C1_Init();
	MX_USB_HOST_Init();
	MX_USART2_UART_Init();
	/* USER CODE BEGIN 2 */

	//Configure Registers of FDC2214
	write_register(RESET, reset);
	write_register(MUX_CONFIG, muxConfig);
	write_register(CLK_DIVIDER_CH0, clkDiv0);
	write_register(CLK_DIVIDER_CH2, clkDiv2);
	write_register(STATUS_CONFIG, errorConfig);
	write_register(RCOUNT_CH0, rcount0);
	write_register(RCOUNT_CH2, rcount2);
	write_register(SETTLECOUNT_CH0, settleCount0);
	write_register(SETTLECOUNT_CH2, settleCount2);
	write_register(DRIVE_CURRENT_CH0, drvCurrent0);
	write_register(DRIVE_CURRENT_CH2, drvCurrent2);
	write_register(CONFIG, config);

	HAL_Delay(500);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		//		int i;
		//		tavg_prev = tavg;
		//		tavg2_prev = tavg2;
		avg_prev = avg;

		//WAIT TO STABILISE
		sample = read_register(CH2MSB, CH2LSB);
		avg = (int)(((int)avg_prev<<N) - avg_prev + sample)>>N;
		delta = avg - avg_prev;

		if (fabs(delta) > DT) {
			integral = integral_prev + delta;
		}
		else {
			integral = integral_prev;
		}

		//************************************
		if ((integral < IT) && (integral > OBJECT)) {
			integral_prev = integral;
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15, GPIO_PIN_SET);	//LED for detection
		}
		else {
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15, GPIO_PIN_RESET);
			integral_prev = integral * LEAKAGE;
		}

		//************************************
		//		printf("%d\t%d\n", (int)integral, (int)avg);
		HAL_Delay(100);


		/*-----------------------------------*ANOTHER OPTION*-----------------------------------------------*/

		//		samples[N-1] = read_register(CH0MSB, CH0LSB);
		//		for(i=1;i<N;i++)
		//		{
		//			samples[i-1] = samples[i];
		//		}
		//		count++;
		//		samples2[N-1] = read_register(CH2MSB, CH2LSB);
		//		for(i=1;i<N;i++)
		//		{
		//			samples2[i-1] = samples2[i];
		//		}
		//		count2++;
		//		if(count>=N)
		//		{
		//			for(i=0;i<N;i++)
		//			{
		//				tavg += samples[i];
		//			}
		//			tavg = tavg/N;
		//
		//			tdelta = tavg - tavg_prev;
		//
		//			if (abs(tdelta) > DT) {
		//				integral = integral_prev + tdelta;
		//			}
		//			else {
		//				integral = integral_prev;
		//			}
		//
		//			if ((integral <= IT) && (integral >= OBJECT)) {
		//				integral_prev = integral;
		//				HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4, GPIO_PIN_SET);	//LED for detection
		//			}
		//			else {
		//				HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4, GPIO_PIN_RESET);
		//				integral_prev = integral * LEAKAGE;
		//			}
		//			count = 0;
		//	}
		//**********************************************************************************************

		//			if(count2>=N)
		//			{
		//				for(i=0;i<N;i++)
		//				{
		//					tavg2 += samples2[i];
		//				}
		//				tavg2 = tavg2/N;
		//
		//				freq2 = (tavg2*fclk)/pow(2,28);
		//				capa = 1/(L*pow(2*M_PI*freq2,2));
		//
		//				tdelta2 = tavg2 - tavg2_prev;
		//
		//				if (abs(tdelta2) > DT2) {
		//					integral2 = integral2_prev + tdelta2;
		//				}
		//				else {
		//					integral2 = integral2_prev;
		//				}
		//
		//				if ((integral2 <= IT2) && (integral2 >= OBJECT2)) {
		//					integral2_prev = integral2;
		//					HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2, GPIO_PIN_SET);	//LED for detection
		//				}
		//				else {
		//					HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2, GPIO_PIN_RESET);
		//					integral2_prev = integral2 * LEAKAGE2;
		//				}
		//
		//
		//				count2 = 0;
		//				//************************************
		//				printf("%d\t\%d\t\%d\t\%d\n",(int)capa,(int)freq2,(int)tavg2,(int)integral2);
		//				//			printf("%d\t%d\n",(int)tdelta,(int)tavg);
		//
		//				HAL_Delay(100);
		//			}
		/*-----------------------------------END *ANOTHER OPTION*-----------------------------------------------*/




		/* USER CODE END WHILE */
		MX_USB_HOST_Process();

		/* USER CODE BEGIN 3 */
		/* USER CODE END 3 */
	}
}
/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

//Function to print using ITM - printf won't work without this
int _write(int file, char *ptr, int len)
{
	int i=0;
	for(i=0;i<len;i++)
	{ITM_SendChar((*ptr++));}
	return len;
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
